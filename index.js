// console.log('JS DOM Manipulation')

// [SECTION] Document Object Model (DOM)
	// allows us to access or modify the properties of an html element in webpage.
	// it is a standard on how to get, change, add or delete HTML elements
	

	// Recap
		//CSS selectors
			//class selector (.);
			//id selector (#);
			//tag selector (html tags)
			//universal selector(*)
			//attribute selector ([attribute])



	// document.querySelector //
	
	// for selecting html elements we will be using document.querySelector/getElementBy Id

		/*Syntax:
			document.querySelector('html element')
	*/


		let universalSelector = document.querySelectorAll('*')
		let singleUniversalSelector = document.querySelector('*')

		console.log(universalSelector);
		console.log(singleUniversalSelector)


		let classSelector = document.querySelectorAll('.full-name')
		console.log(classSelector)


		let singleClassSelector = document.querySelector('.full-name')
		console.log(singleClassSelector)


		// ID
		let idSelector = document.querySelector('#txt-first-name')
		console.log(idSelector)

		// HTML
		let tagSelector = document.querySelectorAll('input')
		console.log(tagSelector)



	// getElement //
		let element = document.getElementById('fullName')
		console.log(element)

		element = document.getElementsByClassName('full-name')
		console.log(element)




// [SECTION]Event Listeners
		// whenever a user interacts with a webpage, this action is considered as an event
		// working with events is large part of creating interactivity in a web page
		// specific function that will be triggered if the event happen


		// The function use is "addEventListener", it takes two arguments
			//1. first argument, is a string identifying the event

				// Events
					// change - element has been changed
					// click - element has been clicked
					// load - the browser has finished loading webpage
					// keydown - keyboard is pushed
					// keyup - keyboard is released after being pushed



			//2. second argument, is a function that the listener will trigger once the specified

		// KeyUp/KeyDown

		// FirstName
		let txtFirstName = document.querySelector('#txt-first-name')
		
		// Target Span
		let spanSelector = document.querySelector('span[id]')
		console.log(spanSelector)

		// Add event listener
		txtFirstName.addEventListener('keyup', ()=>{
		
		spanSelector.innerHTML = `${txtFirstName.value} ${txtLastName.value}`
		
		})


		// LastName
		let txtLastName = document.querySelector('#txt-last-name')

		// Add event listener
		txtLastName.addEventListener('keyup', ()=> {
			spanSelector.innerHTML = `${txtFirstName.value} ${txtLastName.value}`
		})




// Activity

		// Change

		let colorSelector = document.querySelector('#text-color')
		let fullName = document.querySelector('#fullName')


		colorSelector.addEventListener('change', ()=> {
			fullName.style.color = colorSelector.value;

		})



